# Ansible Docker image
#
# Copyright (C) 2018, 2020-2021 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

ARG DEBIAN_IMAGE_TAG=bullseye
FROM debian:${DEBIAN_IMAGE_TAG}
MAINTAINER LSF operations team <ops@libre.space>

ARG ANSIBLE_VERSION

RUN apt-get update \
	&& apt-get -qy install gnupg \
	&& apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367 \
	&& echo "deb http://ppa.launchpad.net/ansible/ansible/ubuntu focal main" > /etc/apt/sources.list.d/ansible.list \
	&& apt-get update \
	&& apt-get -qy install ansible${ANSIBLE_VERSION:+=$ANSIBLE_VERSION} ansible-lint \
	# Workaround for https://github.com/ansible-collections/community.general/pull/3563
	&& apt-get -qy install patch \
	&& sed -i "s/'ipv4.route-metric'$/'ipv4.route-metric',/" /usr/lib/python3/dist-packages/ansible_collections/community/general/plugins/modules/nmcli.py \
	&& cp -a /usr/lib/python3/dist-packages/ansible_collections/community/general/plugins/modules/nmcli.py /usr/lib/python3/dist-packages/ansible_collections/community/general/plugins/modules/net_tools/nmcli.py \
	# End of workaround
	&& rm -rf /var/lib/apt/lists/*
